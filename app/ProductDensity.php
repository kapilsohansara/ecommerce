<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDensity extends Model
{
     
     protected $table = 'product_densities';
     
     protected $fillable = [
        'name', 'description','status'
    ];

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

}
