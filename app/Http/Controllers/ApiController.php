<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Topping;
use App\User;
use App\ProductDensity;
use DB;

class ApiController extends Controller
{
    
    public function __construct()
    {
        
    }

    public function productFirms(Request $request)
    {
        $response = ['status'=>false];
        $pid = $request->get('pid');
        // dd($this->getItemById(1));
        // dd($category_id);
        $densities = ProductDensity::where('product_id',$pid)->where('status',1)->orderBy('category_id')->get(); 
        $productdensities = [];
        foreach($densities as $productdensity){
            $category = $this->getItemById(\Config::get('data.firmcategories'),$productdensity->category_id);
            if (!array_key_exists($category, $productdensities)) {
                $productdensities[$category] = [];
            }
            $productdensities[$category][] = $productdensity;
        }
        $items = Topping::where('status',1)->orderBy('item_id')->get();
        $otheritems = [];
        foreach($items as $item){
            $otheritem = $this->getItemById(\Config::get('data.itemitems'),$item->item_id);
            if (!array_key_exists($otheritem, $otheritems)) {
                $otheritems[$otheritem] = [];
            }
            $otheritems[$otheritem][] = $item;
        }

        $data['productdensities'] = $productdensities;
        $data['otheritems'] = $otheritems;
        if($data){
            $response['status'] = true;
            $response['data'] = $data;
        }
        return response()->json($response);
    }

    public function getItemById($array,$id){
        foreach($array as $key => $category){
            if($category['id']==$id){
                return $array[$key]['name'];
            }
        }
    }

    public function getProductDensity(Request $request){
        $response = ['status'=>false];
        $product_id = $request->get('product_id');
        $densities = ProductDensity::where('product_id',$product_id)->get(['id','name']);
        if(count($densities)>0){
            $response['status'] = true;
            $response['data'] = $densities;
        }
        return response()->json($response);
    }
}
