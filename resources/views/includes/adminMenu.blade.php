 <!-- Left sidebar start -->
    <div class="vertical-nav">

        <!-- Collapse menu starts -->
        <button class="collapse-menu navButton">
            <i class="icon-menu2"></i>
        </button>
        <!-- Collapse menu ends -->

        <!-- Sidebar menu starts -->
        <ul class="menu clearfix">
            <li>
                <a href="{{url('admn/dashboard')}}">
                    <i class="icon-air-play"></i>
                    <span class="menu-item">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{route('products.create')}}">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="menu-item">Add Products</span>
                </a>
            </li>
            <li>
                <a href="{{route('products.index')}}">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="menu-item">Our Products</span>
                </a>
            </li>
             <li>
                <a href="{{route('toppings.index')}}">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="menu-item">Other Items</span>
                </a>
            </li>
            <li>
                <a href="{{route('pricing.index')}}">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="menu-item">Pricing</span>
                </a>
            </li>
            <li>
                <a href="FoamCalculator.html">
                    <i class="fa fa-calculator"></i>
                    <span class="menu-item">Foam Calculator</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-key"></i>
                    <span class="menu-item">Change Password</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="menu-item">Page Title</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="icon-layers4"></i>
                    <span class="menu-item">Page Title</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="menu-item">Page Title</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="icon-layers4"></i>
                    <span class="menu-item">Page Title</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="menu-item">Page Title</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="icon-layers4"></i>
                    <span class="menu-item">Page Title</span>
                </a>
            </li>
        </ul>
        <!-- Sidebar menu ends -->
    </div>
    <!-- Left sidebar end -->