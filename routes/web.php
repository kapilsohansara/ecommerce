<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 //Route::namespace('frontend')->group(function () {
 Route::get('/','LandingController@index')->name('landing');//});
 Route::get('/shapes/{slug}','LandingController@shapefullView')->name('shapefullview');

//Route::resource('login','LoginController');

Route::resource('admn/dashboard','DashboardController');
Route::resource('admn/products','ProductsController');
Route::resource('cart','frontend\CartController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->group(function () {
//dd('sssss');
	Route::middleware('auth')->group(function () {
		Route::get('toppings','ToppingController@index')->name('toppings.index');
		Route::get('toppings/edit/{id}','ToppingController@edit')->name('toppings.edit');
		Route::get('toppings/create','ToppingController@create')->name('toppings.create');
		Route::post('toppings/store','ToppingController@store')->name('toppings.store');
		Route::post('toppings/update','ToppingController@update')->name('toppings.update');
		Route::get('toppings/delete/{id}','ToppingController@delete')->name('toppings.delete');
		Route::get('pricing','PriceCalculatorController@index')->name('pricing.index');
		Route::get('pricing/create','PriceCalculatorController@create')->name('pricing.create');
		Route::post('pricing/store','PriceCalculatorController@store')->name('pricing.store');
		Route::get('pricing/edit/{id}','PriceCalculatorController@edit')->name('pricing.edit');
		Route::post('pricing/update','PriceCalculatorController@update')->name('pricing.update');
		Route::get('pricing/delete/{id}','PriceCalculatorController@delete')->name('pricing.delete');


	});
});

Route::prefix('Api/v1')->group(function () {
	Route::get('product.firms.get','ApiController@productFirms');
	Route::get('get.product.density','ApiController@getProductDensity')->name('get.product.density');
	//Route::get('product.items.get','ApiController@productItems');
});


//'PriceCalculatorController@index');